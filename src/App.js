import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";

import Router from "./routes";
import rootReducers from "./redux/reducer";
import Nav from "./components/navbar";

const store = createStore(rootReducers, applyMiddleware(thunk));

function App() {
  return (
    <Provider store={store}>
      <Nav />
      <Router />
    </Provider>
  );
}

export default App;
