const apiBaseURL = "https://picsum.photos/v2/list";

export const apiCall = async (url='') => {
  const response = await fetch(apiBaseURL + url, {
    method: "GET",
    headers: {
      "Content-type": "application/json",
    },
  });
  const data = await response.json();
  if (response.ok) return data;
  throw data;
};
