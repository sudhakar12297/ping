import { combineReducers } from "redux";
import images from "./images";
import common from "./common";

const rootReducer = combineReducers({
  images,
  common,
});

export default rootReducer;
