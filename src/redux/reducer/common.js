const initialState = {
  onPage: 0,
};

const common = (state = initialState, action) => {
  const { type } = action;
  switch (type) {
    case "INC_PAGE":
      return {
        ...state,
        onPage: state.onPage + 1,
      };
    default:
      return state;
  }
};

export const getPage = (state) => state.common.onPage;

export default common;
