const initialState = {
  images: [],
  isLoading: true,
  isError: false,
};

const images = (state = initialState, action) => {
  const { type, images } = action;
  console.log(state);
  switch (type) {
    case "GET_IMAGES_START":
      return {
        ...state,
        isLoading: true,
        isError: false,
      };
    case "GET_IMAGES_SUCCESS":
      return {
        ...state,
        isLoading: false,
        isError: false,
        images: state.images?.concat(images),
      };
    case "GET_IMAGES_FAILURE":
      return {
        ...state,
        isLoading: false,
        isError: true,
      };
    default:
      return state;
  }
};

export const getImages = (state) => state.images.images;
export const getIsLoading = (state) => state.images.isLoading;
export const getIsError = (state) => state.images.isError;

export default images;
