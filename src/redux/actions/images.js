import { apiCall } from "../../utils/helper";

export const fetchImages = () => {
  return async (dispatch) => {
    dispatch({ type: "GET_IMAGES_START" });
    try {
      const data = await apiCall();
      console.log(data);
      dispatch({ type: "GET_IMAGES_SUCCESS", images: data });
    } catch (e) {
      dispatch({ type: "GET_IMAGES_FAILURE" });
    }
  };
};

export const fetchImagesPagination = () => {
  return async (dispatch, getState) => {
    const {onPage} = getState().common
    dispatch({ type: "GET_IMAGES_START" });
    try {
      const data = await apiCall(`?page=${onPage+1}&limit=10`);
      console.log(data);
      dispatch({type:'INC_PAGE'})
      console.log('data');
      dispatch({ type: "GET_IMAGES_SUCCESS", images: data });
    } catch (e) {
        console.log('error',e);
      dispatch({ type: "GET_IMAGES_FAILURE" });
    }
  };
};
