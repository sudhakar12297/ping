import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Header from "../../components/header";
import DisplayImages from "../../components/displayImages";
import { getImages, getIsLoading } from "../../redux/reducer/images";
import { fetchImagesPagination } from "../../redux/actions/images";

const Home = () => {
  const dispatch = useDispatch();

  const images = useSelector(getImages);
  const isLoading = useSelector(getIsLoading);

  useEffect(() => {
    dispatch(fetchImagesPagination());
  }, []);

  const onClickLoadMore = () => {
    dispatch(fetchImagesPagination());
  };

  return (
    <>
      <Header />
      {isLoading ? (
        "Loading..."
      ) : (
        <div>
          <DisplayImages images={images || []} />
          <button onClick={onClickLoadMore}>Load more</button>
        </div>
      )}
    </>
  );
};

export default Home;
