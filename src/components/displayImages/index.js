import React from "react";

const DisplayImages = (props) => {
  const { images } = props;
  return (
    <>
      {images.length > 0 &&
        images.map((image) => (
          <div>
            <img
              style={{ width: image.width / 10, height: image.height / 10 }}
              src={image.download_url}
            />
          </div>
        ))}
    </>
  );
};

export default DisplayImages;
