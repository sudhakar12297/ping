import React from "react";

const Nav = () => {
  return (
    <div  style={{minHeight:"130px"}}>
      <div style={{float:'left', width:"40%"}}>
        <img
         style={{maxHeight:'100px', maxWidth:'50%'}}
          src={`https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR3gaoI0UnC1y9U6A_G7gBIVqVebxwvk-_1FQ&usqp=CAU`}
        />
      </div>
      <div style={{float:'right', width:"60%"}}>
        <h1>Ping Images</h1>
      </div>
    </div>
  );
};

export default Nav;
